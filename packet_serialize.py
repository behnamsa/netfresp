from helper import *
import struct

def serialize(obj):
    if type(obj) is bytes:
        return obj
    if type(obj) is str:
        return obj.encode('utf-8')
    else:
        return obj.serialize()


# link layer -----------------------------------------------------------------------------------------------------------

class ethernet_frame:
    def __init__(self, source, dest, payload, ethertype=None):
        self.source = source
        self.dest = dest
        self.ethertype = ethertype if ethertype is not None else payload.ethertype
        self.payload = payload

    @classmethod
    def extract(cls, buffer):
        try:
            dest, source, ethertype = unpack('! 6s 6s H', buffer[:14])
            payload = extract_datagram(ethertype, buffer[14:])
            return ethernet_frame(int.from_bytes(source, 'big'), int.from_bytes(dest, 'big'), payload, ethertype)
        except struct.error:
            return buffer

    def serialize(self):
        return pack('! 6s 6s H', int.to_bytes(self.dest, 6, 'big'), int.to_bytes(self.source, 6, 'big'),
                    self.ethertype) + serialize(self.payload)

    def __str__(self):
        lines = [
            f'Ethernet frame:',
            f'    Source MAC address: {mac_to_str(self.source)}',
            f'    Destination MAC address: {mac_to_str(self.dest)}',
            f'    EtherType: 0x{self.ethertype:04X}',
        ]
        string = format_lines(lines, f=self)
        string += format_object('Payload', self.payload)
        return string


# network layer --------------------------------------------------------------------------------------------------------

class ip_datagram:
    def __init__(self, source, dest, payload=b'', protocol=None,
                 # arguments for extraction
                 version=4, header_length=20 // 4, type_of_service=0, total_length=None, identification=0,
                 flags=0, fragment_offset=0, time_to_live=123, header_checksum=None, options=b''):
        self.version = version
        self.header_length = header_length
        self.type_of_service = type_of_service
        self.total_length = len(serialize(payload)) + self.header_length * 4 if total_length is None else total_length
        self.identification = identification
        self.flags = flags
        self.fragment_offset = fragment_offset
        self.time_to_live = time_to_live
        self.protocol = protocol if protocol is not None else payload.protocol
        self.header_checksum = 0
        self.source = source
        self.dest = dest
        self.options = options
        self.payload = payload
        self.header_checksum = self.calculate_checksum() if header_checksum is None else header_checksum

    @classmethod
    def extract(cls, buffer):
        try:
            (version_header_length, type_of_service, total_length, identification,
             flags_fragment_offset, time_to_live, protocol, header_checksum, source, dest
             ) = (unpack('!BBHHHBBHII', buffer[0:20]))
            version, header_length = version_header_length >> 4, version_header_length & 0xf
            flags, fragment_offset = flags_fragment_offset >> 13, flags_fragment_offset & 0b0001111111111111
            options = buffer[20:header_length * 4]
            payload = buffer[header_length * 4:total_length]
            if fragment_offset == 0:
                payload = extract_segment(protocol, payload)
            return ip_datagram(source, dest, payload, protocol, version, header_length, type_of_service, total_length,
                               identification, flags, fragment_offset, time_to_live, header_checksum, options)
        except struct.error:
            return buffer

    def serialize(self):
        return pack('!BBHHHBBHII',
                    self.version << 4 | self.header_length, self.type_of_service,
                    self.total_length, self.identification,
                    self.flags << 13 | self.fragment_offset, self.time_to_live,
                    self.protocol, self.header_checksum,
                    self.source, self.dest) + self.options + serialize(self.payload)

    def calculate_checksum(self):
        return calculate_checksum(self.serialize()[0:self.header_length * 4])

    @property
    def ethertype(self): return 0x0800

    def __str__(self):
        lines = [
            f'IP datagram:',
            f'    Version: {self.version}',
            f'    Header length: {self.header_length}',
            f'    Type of service: {self.type_of_service}',
            f'    Total length: {self.total_length}',
            f'    Identification: {self.identification}',
            f'    Flags: {self.flags} ({ip_flags_to_str(self.flags)})',
            f'    Fragment offset: {self.fragment_offset}',
            f'    Time to live: {self.time_to_live}',
            f'    Protocol: {self.protocol}',
            f'    Header checksum: 0x{self.header_checksum:04X}'
            + f' ({"valid" if self.calculate_checksum() == 0 else "invalid"})',
            f'    Source IP address: {ip_to_str(self.source)}',
            f'    Destination IP address: {ip_to_str(self.dest)}',
        ]
        string = format_lines(lines, d=self)
        string += format_object('Options', self.options)
        string += format_object('Payload', self.payload)
        return string


class icmp_message_base:
    def __init__(self, type, code, checksum=None):
        self.type = type
        self.code = code
        self.checksum = 0
        self.checksum = self.calculate_checksum() if checksum is None else checksum

    @classmethod
    def extract(cls, buffer):
        try:
            type, code = unpack('!BB', buffer[:2])
            return extract_icmp_message(type, code, buffer)
        except struct.error:
            return buffer

    def serialize(self):
        return pack('!BBH', self.type, self.code, self.checksum)

    def calculate_checksum(self):
        return calculate_checksum(self.serialize())

    @property
    def protocol(self):
        return 1

    def __str__(self):
        lines = [
            f'    Type: {self.type}',
            f'    Code: {self.code}',
            f'    Checksum: 0x{self.checksum:04X} ({"valid" if self.calculate_checksum() == 0 else "invalid"})',
        ]
        return format_lines(lines)


class icmp_message(icmp_message_base):
    def __init__(self, type, code, rest=b'\x00'*4, data=b'', checksum=None):
        self.rest = rest
        self.data = data
        super().__init__(type, code, checksum)

    @classmethod
    def extract(cls, buffer):
        try:
            type, code, checksum, rest = unpack('!BBH 4s', buffer[:8])
            data = buffer[8:]
            return icmp_message(type, code, rest, data, checksum)
        except struct.error:
            return buffer

    def serialize(self):
        return super().serialize() + pack('!4s', self.rest) + serialize(self.data)

    def __str__(self):
        lines = [
            f'ICMP message:',
            *super().__str__().splitlines(),
            f'    Rest of header: ' + str(self.rest),
        ]
        string = format_lines(lines)
        string += format_object('Data', self.data)
        return string


class icmp_echo_message(icmp_message_base):
    def __init__(self, identifier, sequence_number, payload=b'', reply=False, checksum=None):
        self.identifier = identifier
        self.sequence_number = sequence_number
        self.payload = payload
        super().__init__(0 if reply else 8, 0, checksum)

    @classmethod
    def extract(cls, buffer):
        try:
            type, code, checksum, identifier, sequence_number = unpack('!BBHHH', buffer[:8])
            assert type in (0, 8) and code == 0
            payload = buffer[8:]
            return icmp_echo_message(identifier, sequence_number, payload, type == 0, checksum)
        except struct.error:
            return buffer

    def serialize(self):
        return super().serialize() + pack('!HH', self.identifier, self.sequence_number) + self.payload

    def __str__(self):
        lines = [
            f'ICMP echo {"request" if self.type == 8 else "reply"}:',
            *super().__str__().splitlines(),
            f'    Identifier: {self.identifier}',
            f'    Sequence number: {self.sequence_number}',
        ]
        string = format_lines(lines)
        string += format_object('Payload', self.payload)
        return string


class arp_datagram:
    def __init__(self, sha, spa, tha, tpa, reply=False, htype=1, ptype=0x0800, hlen=6, plen=4, oper=None):
        self.htype, self.ptype, self.hlen, self.plen = htype, ptype, hlen, plen
        self.oper = oper if oper is not None else 2 if reply else 1
        self.sha, self.spa, self.tha, self.tpa = sha, spa, tha, tpa

    @classmethod
    def extract(cls, buffer):
        try:
            htype, ptype, hlen, plen, oper, sha, spa, tha, tpa = unpack('!HHBBH 6s I 6s I', buffer[:28])
            sha, tha = int.from_bytes(sha, 'big'), int.from_bytes(tha, 'big')
            return arp_datagram(sha, spa, tha, tpa, False, htype, ptype, hlen, plen, oper)
        except struct.error:
            return buffer

    def serialize(self):
        return pack('!HHBBH 6s I 6s I',
                    self.htype, self.ptype, self.hlen, self.plen, self.oper,
                    int.to_bytes(self.sha, 6, 'big'), self.spa, int.to_bytes(self.tha, 6, 'big'), self.tpa)

    @property
    def ethertype(self): return 0x0806

    def __str__(self):
        lines = [
            f'ARP datagram:',
            f'    Hardware type: {self.htype}',
            f'    Protocol type: {self.ptype}',
            f'    Hardware address length: {self.hlen}',
            f'    Protocol address length: {self.plen}',
            f'    Operation: {self.oper} ({"reply" if self.oper == 2 else "request"})',
            f'    Sender hardware address: {mac_to_str(self.sha)}',
            f'    Sender protocol address: {ip_to_str(self.spa)}',
            f'    Target hardware address: {mac_to_str(self.tha)}',
            f'    Target protocol address: {ip_to_str(self.tpa)}',
        ]
        return format_lines(lines)


# transport layer ------------------------------------------------------------------------------------------------------

class tcp_segment:
    def __init__(self, source_addr, dest_addr, source_port, dest_port,
                 sequence_number, ack_number, flags, payload=b'',
                 data_offset=20 // 4, window_size=65535, checksum=None, urgent_pointer=0, options=b''):
        self.source, self.dest = source_port, dest_port
        self.sequence_number, self.ack_number = sequence_number, ack_number
        self.data_offset, self.flags, self.window_size = data_offset, TcpFlags(flags), window_size
        self.checksum, self.urgent_pointer = 0, urgent_pointer
        self.options, self.payload = options, payload
        self.checksum = self.calculate_checksum(source_addr, dest_addr) if checksum is None else checksum

    @classmethod
    def extract(cls, buffer):
        try:
            (source, dest, sequence_number, ack_number, data_offset_flags, window_size,
             checksum, urgent_pointer) = unpack('!HHIIHHHH', buffer[:20])
            data_offset, flags = data_offset_flags >> 12, TcpFlags(data_offset_flags & 0x0fff)
            options = buffer[20:data_offset * 4]
            payload = extract_application_data(min(source, dest), buffer[data_offset * 4:])
            return tcp_segment(None, None, source, dest, sequence_number, ack_number, flags, payload,
                               data_offset, window_size, checksum, urgent_pointer, options)
        except struct.error:
            return buffer

    def serialize(self):
        return pack('!HHIIHHHH', self.source, self.dest, self.sequence_number, self.ack_number,
                    self.data_offset << 12 | self.flags, self.window_size, self.checksum, self.urgent_pointer
                    ) + self.options + serialize(self.payload)

    def calculate_checksum(self, source_addr, dest_addr):
        pseudo_header = pack('!IIBBH', source_addr, dest_addr, 0, 6, self.data_offset * 4 + len(self.payload))
        return calculate_checksum(pseudo_header + self.serialize())

    @property
    def protocol(self):
        return 6

    def __str__(self):
        lines = [
            f'TCP segment:',
            f'    Source port: {self.source}',
            f'    Destination port: {self.dest}',
            f'    Sequence number: {self.sequence_number}',
            f'    Acknowledgment number: {self.ack_number}',
            f'    Data offset: {self.data_offset}',
            f'    Flags: {self.flags} ({int(self.flags)})',
            f'    Window Size: {self.window_size}',
            f'    Checksum: 0x{self.checksum:04X}',
            f'    Urgent pointer: {self.urgent_pointer}',
        ]
        string = format_lines(lines)
        string += format_object('Options', self.options)
        string += format_object('Payload', self.payload)
        return string


class udp_segment:
    def __init__(self, source_addr, dest_addr, source_port, dest_port, payload=b'', length=None, checksum=None):
        self.source, self.dest = source_port, dest_port
        self.length, self.checksum = 8 + len(serialize(payload)) if length is None else length, 0
        self.payload = payload
        self.checksum = self.calculate_checksum(source_addr, dest_addr) if checksum is None else checksum

    @classmethod
    def extract(cls, buffer):
        try:
            source, dest, length, checksum = unpack('!4H', buffer[:8])
            payload = extract_application_data(min(source, dest), buffer[8:length])
            return udp_segment(None, None, source, dest, payload, length, checksum)
        except struct.error:
            return buffer

    def serialize(self):
        return pack('!4H', self.source, self.dest, self.length, self.checksum) + serialize(self.payload)

    def calculate_checksum(self, source_addr, dest_addr):
        pseudo_header = pack('!IIBBH', source_addr, dest_addr, 0, 17, 8 + len(serialize(self.payload)))
        return calculate_checksum(pseudo_header + self.serialize())

    @property
    def protocol(self):
        return 17

    def __str__(self):
        lines = [
            f'UDP segment:',
            f'    Source port: {self.source}',
            f'    Destination port: {self.dest}',
            f'    Length: {self.length}',
            f'    Checksum: 0x{self.checksum:04X}',
        ]
        string = format_lines(lines)
        string += format_object('Payload', self.payload)
        return string


# application layer ----------------------------------------------------------------------------------------------------

class dns_message:
    def __init__(self, identifier, flags, questions=[], answers=[], nameservers=[], additionals=[]):
        self.identifier, self.flags = identifier, DnsHeaderFlags(flags)
        self.questions, self.answers = questions, answers
        self.nameservers, self.additionals = nameservers, additionals

    @classmethod
    def extract(cls, buffer):
        try:
            identifier, flags, qdcount, ancount, nscount, arcount = unpack_from('!6H', buffer)
            offset = 12
            questions, answers, nameservers, additionals = [], [], [], []

            for i in range(qdcount):
                q, offset = DnsQuestionEntry.extract(buffer, offset)
                questions.append(q)
            for lst in ((answers, ancount), (nameservers, nscount), (additionals, arcount)):
                for i in range(lst[1]):
                    rr, offset = DnsRREntry.extract(buffer, offset)
                    lst[0].append(rr)

            return dns_message(identifier, flags, questions, answers, nameservers, additionals)
        except struct.error:
            return buffer

    def serialize(self):
        buffer = pack('!6H', self.identifier, self.flags,
                      len(self.questions), len(self.answers), len(self.nameservers), len(self.additionals))

        for lst in (self.questions, self.answers, self.nameservers, self.additionals):
            for rr in lst:
                buffer += rr.serialize()

        return buffer

    def __str__(self):
        lines = [
            f'DNS message:',
            f'    Identifier: {self.identifier}',
            f'    Flags: {self.flags} ({int(self.flags)})',
        ]
        string = format_lines(lines)
        string += format_object('Questions', self.questions)
        string += format_object('Answers', self.answers)
        string += format_object('Name servers', self.nameservers)
        string += format_object('Additional records', self.additionals)
        return string


class http_message:
    def __init__(self, message):
        self.message = message

    @classmethod
    def extract(cls, buffer):
        buffer = buffer.decode('utf-8', 'backslashreplace')
        return http_message(buffer)

    def serialize(self):
        return serialize(self.message)

    def __str__(self):
        return format_object('HTTP message', self.message, indent=0)


# generic extraction functions -----------------------------------------------------------------------------------------

def extract_frame(buffer):
    return ethernet_frame.extract(buffer)

def extract_datagram(protocol, buffer):
    if protocol == 0x0800:
        return ip_datagram.extract(buffer)
    if protocol == 0x0806:
        return arp_datagram.extract(buffer)
    # unknown protocol
    return buffer

def extract_icmp_message(type, code, buffer):
    if (type == 0 or type == 8) and code == 0:
        return icmp_echo_message.extract(buffer)
    # unknown type
    return icmp_message.extract(buffer)

def extract_segment(protocol, buffer):
    if protocol == 1:
        return icmp_message_base.extract(buffer)
    if protocol == 6:
        return tcp_segment.extract(buffer)
    if protocol == 17:
        return udp_segment.extract(buffer)
    # unknown protocol
    return buffer

def extract_application_data(port, buffer):
    if port == 53:
        return dns_message.extract(buffer)
    if port == 80:
        return http_message.extract(buffer)
    # unknown protocol
    return buffer
